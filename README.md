# README for MKOS-EXTRAS#

Most items inside this repository ship with their own build or install instructions. You will have to read them to know what to do.

### What is this repository for? ###

Repository containing extras for MkOS. Drivers, install scripts, distributions, binaries, utility applications, and more will be published here.

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* We do not take contributions directly to this repository. This is a collection of other contributions. It is managed by the administration of MkOS. 
	To have your contribution placed here, it must have officially reached MkOS "Extra" status.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact